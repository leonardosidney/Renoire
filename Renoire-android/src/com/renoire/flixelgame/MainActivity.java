package com.renoire.flixelgame;

import org.flixel.FlxAndroidApplication;

public class MainActivity extends FlxAndroidApplication {
    public MainActivity() {
		super(new RenoireMain());
	}
}