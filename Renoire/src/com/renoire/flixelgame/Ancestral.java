package com.renoire.flixelgame;

import org.flixel.FlxG;
import org.flixel.FlxGroup;
import org.flixel.FlxSprite;

public class Ancestral extends FlxSprite {
	public int borda = 40, vida = 10;
	private float pos_x,pos_y;
	private static final String imgAncestral = "ancestral.png";
	private FlxGroup inimigos;
	public Ancestral(FlxGroup e) {
		super(FlxG.width*0.02f,FlxG.height*0.45f,imgAncestral);
		pos_x=FlxG.width*0.02f;
		pos_y=FlxG.height*0.45f;
		
		inimigos = e;
	}
	public float getX() {
		return this.pos_x;
	}
	public float getY() {
		return this.pos_y;
	}
	public int getBorda(){
		return this.borda;
	}
	public void update() {
		if(pos_x != x|| pos_y != y) {
			x = pos_x;
			y = pos_y;
		}
		if(vida <= 0) {
			this.kill();
		}
		super.update();
	}
}
