package com.renoire.flixelgame;

import org.flixel.FlxG;
import org.flixel.FlxSprite;

import java.util.Random;

public class Enemy extends FlxSprite {
	private static Random r = new Random();
	private static final String enemyImg = "inimigo.png";
	private float x1,x2,y1,y2,vx,vy,vu;
	private int pontos;
	private PlayState game,borda;
	private Ancestral ancient;
	private Player jogador;
	public Enemy(PlayState state, Ancestral base, Player p) {
		super(FlxG.width,FlxG.height*(r.nextFloat())+20);
		this.makeGraphic(45, 30);
		this.loadGraphic(enemyImg, true, true, 40, 30);
		// animation name, frames, frame rate, looped.
		this.addAnimation("walk", new int[]{0,2}, 3, true);
		game = state;
		ancient = base;
		jogador = p;
		pontos = 10;
	}
	public void update(){
		int b = 28;
		//Não sei porque não estava dando colisão,então resolvi "escrever na mão"
		if(this.x - 10 < ancient.x + b && this.x > ancient.x - b &&
				this.y - 10 < ancient.y + b && this.y + 10 > ancient.y - b) {
			this.kill();
			ancient.vida--;
		}
		x1 = x;
		y1 = y;
		x2 = ancient.getX();
		y2 = ancient.getY()+(ancient.getBorda()/2);
		vx = x2 - x1;
		vy = y2 - y1;
		vu = (float)Math.sqrt(Math.pow(vx, 2)+Math.pow(vy, 2));
		vx = (vx / vu)*game.getEnemyVel();
		vy = (vy / vu)*game.getEnemyVel();
		velocity.x = vx;
		velocity.y = vy;
		if(FlxG.collide(jogador,this)) {
			game.AumentaPontos(pontos);
			this.kill();
		}
		this.play("walk");
		super.update();
	}
}
