package com.renoire.flixelgame;

import org.flixel.FlxG;
import org.flixel.FlxState;
import org.flixel.FlxText;

public class FimDeJogo extends FlxState {
	public void create() {
		FlxText titulo = new FlxText(FlxG.width*0.5f, FlxG.height*0.5f-40, 0, "Game Over");
		titulo.setFormat(null,18,0xffffff,"center");
		add(titulo);
	}
	public void update() {
		if(FlxG.mouse.pressed()) {
			this.kill();
			FlxG.switchState(new MenuState());
		}
		super.update();
	}
}
