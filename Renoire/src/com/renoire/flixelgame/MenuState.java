package com.renoire.flixelgame;

import org.flixel.FlxG;
import org.flixel.FlxState;
import org.flixel.FlxText;

public class MenuState extends FlxState {
	public void create() {
		FlxText titulo = new FlxText(FlxG.width*0.5f, FlxG.height*0.5f-40, 0, "Renoire");
		FlxText textPlay = new FlxText(FlxG.width*0.5f, FlxG.height*0.5f, 0, "Tap to play");
		titulo.setFormat(null,18,0xffffff,"center");
		textPlay.setFormat(null,18,0xffffff,"center");
		add(titulo);
		add(textPlay);
	}
	public void update() {
		if(FlxG.mouse.pressed()) {
			this.kill();
			FlxG.switchState(new PlayState());
		}
		super.update();
	}
}