package com.renoire.flixelgame;

import java.util.Random;

import org.flixel.FlxG;
import org.flixel.FlxGroup;
import org.flixel.FlxState;
import org.flixel.FlxText;

public class PlayState extends FlxState{
	private Player jogador;
	private FlxGroup inimigos;
	private Ancestral ancient;
	private Mapa fundo;
	private int nivel,enemyVel,pontos,chance,lvlUp;
	private Random r = new Random();
	private Enemy e;
	private FlxText pontosTotais;
	public PlayState() {
		this.nivel = 1;
		this.chance = 800;
		this.pontos = 0;
		this.lvlUp = 0;
	}
	public void create() {
		fundo = new Mapa();
		add(fundo);
		jogador = new Player();
		add(jogador);
		ancient = new Ancestral(inimigos);
		add(ancient);
		inimigos = new FlxGroup(10);
	}
	public void update() {
		for(int i = 0; i < 10;i++) {
			if(inimigos.getFirstNull() == -1) {
				if(r.nextInt() % chance == 0) {
					e = new Enemy(this,ancient,jogador);
					inimigos.add(e);
				}
			}
			if(r.nextInt() % chance == 0) {
				inimigos.replace(inimigos.getFirstDead(),new Enemy(this,ancient,jogador));
			}
		}
		if(lvlUp >= 100) {
			this.nivel++;
			this.lvlUp = 0;
			this.chance -= 50;
		}
		add(inimigos);
		this.enemyVel = 20*nivel;
		FlxG.collide(jogador,ancient);
		remove(pontosTotais);
		pontosTotais = new FlxText(FlxG.height+40,FlxG.height*0.02f,0,("Vidas: "+String.valueOf(ancient.vida) +" Pontos: " + String.valueOf(pontos)));
		pontosTotais.setFormat(null,18,0xffffff,"right");
		add(pontosTotais);
		if(ancient.vida == 0) {
			this.kill();
			FlxG.switchState(new FimDeJogo());
		}
		super.update();
	}
	public int getEnemyVel() {
		return this.enemyVel;
	}
	public void AumentaPontos(int i) {
		this.pontos += i;
		this.lvlUp  += 5;
	}
}