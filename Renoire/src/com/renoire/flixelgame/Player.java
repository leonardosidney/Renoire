package com.renoire.flixelgame;

import org.flixel.FlxG;
import org.flixel.FlxSprite;

public class Player extends FlxSprite {
	public static final String playerImg = "player.png";
	private float x1,x2,y1,y2,vx,vy,vu;
	public int borda = 20;
	public Player() {
		
		//this.makeGraphic(20, 20);
		// Path of the image, animated, reverse support, width and height of the frame.
		super(50,50);
		this.makeGraphic(30, 30);
		this.loadGraphic("Hero_animate.png", true, true, 21, 30);
		// animation name, frames, frame rate, looped.
		this.addAnimation("stand", new int[]{0},0, false);
		this.addAnimation("walk_r", new int[]{10,12}, 4, true);
		this.addAnimation("walk_l", new int[]{6,8}, 4, true);
		this.play("stand");
	}
	public void update() {
		
		if(FlxG.mouse.justPressed()) {
			x1 = x;
			y1 = y;
			x2 = FlxG.mouse.screenX;
			y2 = FlxG.mouse.screenY;
			vx = x2 - x1;
			vy = y2 - y1;
			vu = (float)Math.sqrt(Math.pow(vx, 2)+Math.pow(vy, 2));
			vx = (vx / vu)*80;
			vy = (vy / vu)*80;
			velocity.x = vx;
			velocity.y = vy;
			if(x < x2) {
				this.play("walk_r");
			}
			if((x > x2)){
				this.play("walk_l");
			}
		}
		if((x < x2&& x1 > x2)||(x > x2 && x1 < x2)) {
			velocity.x = 0;
			this.play("stand");
		}
		if((y < y2&& y1 > y2)||(y > y2 && y1 < y2)) {
			this.play("stand");
			velocity.y = 0;
		}
		//double ab = Math.sqrt(Math.pow((FlxG.mouse.screenX-x), 2)+Math.pow((FlxG.mouse.screenY - y),2));
		super.update();
	}
}
